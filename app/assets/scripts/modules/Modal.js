import $ from "jquery";
class Modal{
    constructor(){
        this.openModalButton = $(".open-modal");
        this.modal = $(".modal");
        this.modalCloseButton = $(".modal-close");
        this.events();
    }
    events() {
        //bind is use to bind this
        //()=> arrow operator auto binds 
        this.openModalButton.click(this.openModal.bind(this));
        this.modalCloseButton.click(this.closeModal.bind(this));
        $(document).keyup(this.keyPressHandler.bind(this));
    }
    
    openModal(){
        this.modal.addClass("modal-is-visible");
        return false;
    }
    closeModal(){
        this.modal.removeClass("modal-is-visible");
    }
    keyPressHandler(e) {
        if(e.keyCode == 27) {
            this.closeModal();
        }
    }
}

export default Modal;